#!/bin/bash

echo "Clearing users.txt, login_list.txt, scores.txt, teams.txt, solved.txt"
echo "" > ../accounts/teams.txt
echo "" > ../accounts/scores.txt
echo "" > ../accounts/login_list.txt
echo "" > ../accounts/solved.txt
echo "" > ../accounts/users.txt
echo "Done!"
